# Live Praise ( Software para Projeção em Igrejas) #


Live Praise é um software dedicado a exibição de Louvores, Biblias, Videos e outras atividades na igreja.
Com ele é possível realizar as tarefas essênciais para o bom andamento do culto de forma fácil e rápida, permitindo realizar operações Ao Vivo, que esteja fora do planejamento inicial, com agilidade.


## Requisitos Mínimos ##


Para o funcionamento do sistema é necessário que ele atenda os requisitos minimos.


**Processador:** Intel Pentium 4 ou posterior compatível com SSE2


**Sistema Operacional:** Windows 7 ou Superior, Mac OS X Yosemite 10.10 ou Superior, Debian 8+, openSUSE 13.3+, Fedora Linux 24+ ou Ubuntu 14.04+ de 64 bits


## Recursos do Sistema ##

- Multiplos Monitores
- Fundo de tela usando Imagens ou Vídeos
- Executar conteúdo para o Operador sem afetar o que é exibido ao Público
- Salvar Músicas para Exibição Posterior
- Exibição de Músicas
- Exibição de Biblias